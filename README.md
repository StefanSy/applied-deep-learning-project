# Applied Deep Learning Project

## Project description

### Motivation

In the last few years, two of the biggest breakthroughs in Machine Learning were Generative Adversarial Networks (GANs) in Computer Vision and Transformer networks in Natural Language Processing. Transformer networks became pretty much the standard model for transforming language in any way, for example translating an english into a german sentence ([Google Translator](https://translate.google.com)). GANs introduced a powerful unsupervised approach to train any generative model with very promising results ([This Person does not exist](https://thispersondoesnotexist.com)).<br>
In this project we combine these popular methods by training a Transformer model in an adversarial fashion. We apply the generative model as a conversational chatbot. The idea of training the bot using a GAN comes natural, since the ultimate goal of a chatbot is to appear real or to fool any real user, which is the essential idea behind GAN training.

### Deep Learning Approach

The idea is to use two transformer networks as Generator and Discriminator. Furthermore, since the goal is to generate answers depending on a question, we use a conditional GAN. Hence, we have an conditional input (the question) for both Generator and Discriminator. To introduce some randomness to the answers, an additional random latent vector is fed into the Generator. This allows the chatbot to appear more human, since a human probably would not answer with the identical sentence to the same question. In the Generator, we utilise a classic Transformer network which outputs for a given input sequence and the start of an output sequence the next most likely word. As Discriminator we need a classifier network, which outputs whether the combination of a question-answer pair is real or synthetic.
<br>
In a GAN, both networks are trained simultaneously. The Discriminator is fed real samples from some chatbot dataset and synthetic samples from the generative model and then outputs a realness-probability. The Discriminator's loss function is therefore the Binary Crossentropy Loss. At the same time, the Generator must become better at generating synthetic samples to fool the Discriminator. Hence, the Generator receives a penalty if the Discriminator gives the synthetic samples a low realness probability. This forms a min-max game where the Generator tries to maximize the realness probability for its samples while the Discriminator minimizes this probability. This leads to both the Generator and Discriminator increasing their performance and theoretically ending up in a nash equilibrium where the Generator produces perfect samples and the Discriminator is unable to decide which sample is synthetic and which is real.
<br>
However, the last n-1 questions are not considered when answering the n-th question. So the model will not be able to have a real full conversation, but will "reset" after each question. A full conversation bot would likely be possible, but that is out of scope for this project and would therefore be possible future work. 
<br>
For more information see the [documentation](./TransGAN.pdf)
![model](./img/transgan.png) 


## Related Projects/Papers

Generative Adversarial Networks
* [Paper: Generative Adversarial Networks](https://arxiv.org/abs/1406.2661)

Transformer Networks
* [Paper: Attention is all you need](https://arxiv.org/abs/1706.03762)

Chatbot based on Transformer networks
* [A Transformer Chatbot Tutorial with TensorFlow 2.0](https://medium.com/tensorflow/a-transformer-chatbot-tutorial-with-tensorflow-2-0-88bf59e66fe2)

## Run git project in Kaggle
* [run in kaggle](https://www.kaggle.com/simonlangrieger/nlp-project)

## Model 1: 
![model](./img/model_1.png) 
