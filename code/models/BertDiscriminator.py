import tensorflow_hub as hub
import tensorflow as tf
from official.nlp import optimization

# we also implemented a Bert Discriminator but decided not to use it
# the code is not commented since it is directly from a tensorflow tutorial where it is explained in more detail
# from https://www.tensorflow.org/text/tutorials/bert_glue#loading_models_from_tensorflow_hub

preprocess_url = 'https://tfhub.dev/tensorflow/bert_en_uncased_preprocess/3'
bert_url = 'https://tfhub.dev/google/electra_small/2'


def make_bert_preprocess_model(sentence_features, seq_length=128):
    """
    Loads a bert model from tensorflow hub
    :param sentence_features:
    :param seq_length:
    :return: bert model
    """
    input_segments = [
        tf.keras.layers.Input(shape=(), dtype=tf.string, name=ft)
        for ft in sentence_features]

    bert_preprocess = hub.load(preprocess_url)
    tokenizer = hub.KerasLayer(bert_preprocess.tokenize, name='tokenizer')
    segments = [tokenizer(s) for s in input_segments]

    packer = hub.KerasLayer(bert_preprocess.bert_pack_inputs,
                            arguments=dict(seq_length=seq_length),
                            name='packer')
    model_inputs = packer(segments)
    return tf.keras.Model(input_segments, model_inputs)


def build_discriminator_model(num_classes):
    inputs = dict(
        input_word_ids=tf.keras.layers.Input(shape=(None,), dtype=tf.int32),
        input_mask=tf.keras.layers.Input(shape=(None,), dtype=tf.int32),
        input_type_ids=tf.keras.layers.Input(shape=(None,), dtype=tf.int32),
    )

    encoder = hub.KerasLayer(bert_url, trainable=True, name='encoder')
    net = encoder(inputs)['pooled_output']
    net = tf.keras.layers.Dropout(rate=0.1)(net)
    net = tf.keras.layers.Dense(num_classes, activation=None, name='classifier')(net)
    return tf.keras.Model(inputs, net, name='prediction')


def get_full_model(preprocess, discriminator):
    preprocess_inputs = preprocess.inputs
    discriminator_inputs = preprocess(preprocess_inputs)
    discriminator_ouputs = discriminator(discriminator_inputs)
    return tf.keras.Model(preprocess_inputs, discriminator_ouputs)


def train_discriminator(preprocessor, discriminator, ds_withInfo, epochs=10, batch_size=32, init_lr=2e-5):
    train_dataset, train_data_size = load_dataset_from_tfds(
        ds_withInfo[0], ds_withInfo[1], 'train', batch_size, preprocessor)
    steps_per_epoch = train_data_size // batch_size
    num_train_steps = steps_per_epoch * epochs
    num_warmup_steps = num_train_steps // 10

    validation_dataset, validation_data_size = load_dataset_from_tfds(
        ds_withInfo[0], ds_withInfo[1], 'validation', batch_size,
        preprocessor)
    validation_steps = validation_data_size // batch_size

    optimizer = optimization.create_optimizer(
        init_lr=init_lr,
        num_train_steps=num_train_steps,
        num_warmup_steps=num_warmup_steps,
        optimizer_type='adamw')

    discriminator.compile(optimizer=optimizer,
                          loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
                          metrics=['accuracy'])

    discriminator.fit(
        x=train_dataset,
        validation_data=validation_dataset,
        steps_per_epoch=steps_per_epoch,
        epochs=epochs,
        validation_steps=validation_steps)


#from https://www.tensorflow.org/text/tutorials/bert_glue#choose_a_task_from_glue
def load_dataset_from_tfds(in_memory_ds, info, split, batch_size, bert_preprocess_model):
    is_training = split.startswith('train')
    dataset = tf.data.Dataset.from_tensor_slices(in_memory_ds[split])
    num_examples = info.splits[split].num_examples

    if is_training:
      dataset = dataset.shuffle(num_examples)
      dataset = dataset.repeat()
    dataset = dataset.batch(batch_size)
    dataset = dataset.map(lambda ex: (bert_preprocess_model(ex), ex['label']))
    dataset = dataset.cache().prefetch(buffer_size=tf.data.AUTOTUNE)
    return dataset, num_examples
