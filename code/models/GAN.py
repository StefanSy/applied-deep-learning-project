# based on https://keras.io/examples/generative/conditional_gan/#creating-a-conditionalgan-model
import tensorflow as tf
import tensorflow.keras as keras
from models.layers.MaskingLayer import MaskingLayer


class ConditionalGAN(keras.Model):
    def __init__(self, generator, discriminator, embedding, discrete=True):
        super(ConditionalGAN, self).__init__()
        self.discriminator = discriminator
        self.generator = generator
        self.embedding = embedding
        self.embedding.trainable = False
        self.discrete = discrete
        self.masking = MaskingLayer()
        self.gen_loss_tracker = keras.metrics.Mean(name="generator_loss")
        self.disc_loss_tracker = keras.metrics.Mean(name="discriminator_loss")

    @property
    def metrics(self):
        return [self.gen_loss_tracker, self.disc_loss_tracker]

    def compile(self, d_optimizer, g_optimizer, loss_fn):
        super(ConditionalGAN, self).compile()
        self.d_optimizer = d_optimizer
        self.g_optimizer = g_optimizer
        self.loss_fn = loss_fn


    def train_step(self, data):
        # Unpack the data. input already tokenized
        # shape (batch, seq_len)
        enc_input, dec_input = data
        enc_padding_mask, dec_target_padding_mask, dec_padding_mask = self.masking(enc_input, dec_input, True)
        # apply embedding on input - embedding is the same in the whole network
        # shape (batch, seq_len, embed_dim)
        enc_input_embed, dec_input_embed = self.embedding(enc_input, False), self.embedding(dec_input, False)

        # Sample random points in the latent space and add to the decoder input.
        batch_size = tf.shape(dec_input_embed)[0]
        random_latent_vectors = tf.random.normal(shape=tf.shape(dec_input_embed), stddev=0.01)
        dec_input_w_latent = dec_input_embed + random_latent_vectors

        # generate fake text.
        gen_batch = (enc_input_embed, dec_input_w_latent, enc_padding_mask, dec_target_padding_mask, dec_padding_mask)
        generated_text = self.generator(gen_batch, training=False)

        # create masks for generated text and conditional input for Discriminator
        enc_padding_mask_fake, dec_target_padding_mask_fake, dec_padding_mask_fake = self.masking(
            tf.argmax(generated_text, axis=-1), enc_input, False)
        enc_padding_mask_real, dec_target_padding_mask_real, dec_padding_mask_real = self.masking(dec_input, enc_input,
                                                                                                  False)
        # build batch of real and fake text together with conditional input and masks
        fake_text_and_input = tf.concat([self.embedding(generated_text, True), dec_input_embed], 0)
        real_text_and_input = tf.concat([enc_input_embed, enc_input_embed], 0)
        enc_padding_mask = tf.concat([enc_padding_mask_real, enc_padding_mask_fake], 0)
        dec_target_padding_mask = tf.concat([dec_target_padding_mask_real, dec_target_padding_mask_fake], 0)
        dec_padding_mask = tf.concat([dec_padding_mask_real, dec_padding_mask_fake], 0)
        combined_text_batch = (
            fake_text_and_input, real_text_and_input, enc_padding_mask, dec_target_padding_mask, dec_padding_mask)

        # Assemble labels discriminating real from fake text.
        labels = tf.concat(
            [tf.zeros((batch_size, 1)), tf.ones((batch_size, 1))], axis=0
        )

        # Train the discriminator.
        with tf.GradientTape() as tape:
            predictions = self.discriminator(combined_text_batch, training=True)
            d_loss = self.loss_fn(labels, predictions)
        grads = tape.gradient(d_loss, self.discriminator.trainable_weights)
        self.d_optimizer.apply_gradients(
            zip(grads, self.discriminator.trainable_weights)
        )

        # build new masks with lookahead
        enc_padding_mask_real, dec_target_padding_mask_real, dec_padding_mask_real = self.masking(enc_input, dec_input,
                                                                                                  True)

        # Sample random points in the latent space and add to input
        random_latent_vectors = tf.random.normal(shape=tf.shape(dec_input_embed), stddev=0.01)
        dec_input_w_latent = dec_input_embed + random_latent_vectors
        generator_batch = (
            enc_input_embed, dec_input_w_latent, enc_padding_mask_real, dec_target_padding_mask_real,
            dec_padding_mask_real)

        # Assemble labels that say "all real text".
        misleading_labels = tf.zeros((batch_size, 1))

        # Train the generator (note that we should *not* update the weights of the discriminator!)
        with tf.GradientTape() as tape:
            generated_text = self.generator(generator_batch, training=True)
            enc_padding_mask_fake, dec_target_padding_mask_fake, dec_padding_mask_fake = self.masking(
                tf.argmax(generated_text, axis=-1), enc_input, False)
            discrim_batch = (
                self.embedding(generated_text), enc_input_embed, enc_padding_mask_fake, dec_target_padding_mask_fake,
                dec_padding_mask_fake)
            predictions = self.discriminator(discrim_batch, training=False)
            g_loss = self.loss_fn(misleading_labels, predictions)
        grads = tape.gradient(g_loss, self.generator.trainable_weights)
        self.g_optimizer.apply_gradients(zip(grads, self.generator.trainable_weights))

        # return and track loss.
        self.gen_loss_tracker.update_state(g_loss)
        self.disc_loss_tracker.update_state(d_loss)
        return {
            "g_loss": self.gen_loss_tracker.result(),
            "d_loss": self.disc_loss_tracker.result(),
        }
