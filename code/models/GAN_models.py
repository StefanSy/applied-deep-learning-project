import tensorflow as tf
import tensorflow_hub as hub

from models.layers.transformer import Transformer
from models.layers.STArgmax import STArgmax

# build the models for the GAN (Generator and Discriminator), using the functional API from keras

def get_models(vocab_size, discrete=True, t_num_layers=5, t_embed_dim=512, t_dense_dim=2048, t_num_heads=8,
               t_pe_input=10000, t_pe_target=10000, t_do_rate=0.1):
    discriminator = get_discriminator(vocab_size, t_num_layers, t_embed_dim, t_dense_dim, t_num_heads, t_pe_input,
                                      t_pe_target, t_do_rate)
    generator = get_generator(vocab_size, t_num_layers, t_embed_dim, t_dense_dim, t_num_heads, t_pe_input, t_pe_target,
                              t_do_rate, discrete)
    return generator, discriminator

def get_discriminator(vocab_size, t_num_layers=5, t_embed_dim=512, t_dense_dim=2048, t_num_heads=8, t_pe_input=10000,
                      t_pe_target=10000, t_do_rate=0.1):
    transformer = Transformer(num_layers=t_num_layers,
                              embed_dim=t_embed_dim,
                              dense_dim=t_dense_dim,
                              num_heads=t_num_heads,
                              vocab_size=vocab_size,
                              target_size=1,
                              pe_input=t_pe_input,
                              pe_target=t_pe_target,
                              do_rate=t_do_rate)

    # in the functional API the Inputs have to defined individually
    enc_input = tf.keras.Input(shape=(None, t_embed_dim), dtype=tf.float32, name="enc_input")
    dec_input = tf.keras.Input(shape=(None, t_embed_dim), dtype=tf.float32, name="dec_input")
    enc_padding_mask = tf.keras.Input(shape=(1, 1, None), dtype=tf.float32, name="enc_padding_mask")
    dec_target_padding_mask = tf.keras.Input(shape=(1, 1, None), dtype=tf.float32, name="dec_target_padding_mask")
    dec_padding_mask = tf.keras.Input(shape=(1, 1, None), dtype=tf.float32, name="dec_padding_mask")

    x = transformer((enc_input, dec_input), enc_padding_mask=enc_padding_mask, look_ahead_mask=dec_target_padding_mask,
                    dec_padding_mask=dec_padding_mask)

    # when calling the transformer we get an ouput for every word of the input, so we just take the average over all
    # to get the final realness probability, This is a rather naive approach but we found that it already works
    x = tf.math.reduce_mean(x, axis=1)
    output = tf.keras.activations.sigmoid(x)
    model = tf.keras.models.Model(
        inputs=[enc_input, dec_input, enc_padding_mask, dec_target_padding_mask, dec_padding_mask], outputs=output)
    return model


def get_generator(vocab_size, t_num_layers=5, t_embed_dim=512, t_dense_dim=2048, t_num_heads=8, t_pe_input=10000,
                  t_pe_target=10000, t_do_rate=0.1, discrete=True):
    # the output size of the transformer depends on the approach. when we use the fully continuous approach the
    # transformer outputs embedding vectors and in the STE approach one-hot vectors over the vocabulary
    if discrete:
        target_size = vocab_size
    else:
        target_size = t_embed_dim

    transformer = Transformer(num_layers=t_num_layers,
                              embed_dim=t_embed_dim,
                              dense_dim=t_dense_dim,
                              num_heads=t_num_heads,
                              vocab_size=vocab_size,
                              target_size=target_size,
                              pe_input=t_pe_input,
                              pe_target=t_pe_target,
                              do_rate=t_do_rate)

    # in the functional API the Inputs have to defined individually
    enc_input = tf.keras.Input(shape=(None, t_embed_dim), dtype=tf.float32, name="enc_input")
    dec_input = tf.keras.Input(shape=(None, t_embed_dim), dtype=tf.float32, name="dec_input")
    enc_padding_mask = tf.keras.Input(shape=(1, 1, None), dtype=tf.float32, name="enc_padding_mask")
    dec_target_padding_mask = tf.keras.Input(shape=(1, 1, None), dtype=tf.float32, name="dec_target_padding_mask")
    dec_padding_mask = tf.keras.Input(shape=(1, 1, None), dtype=tf.float32, name="dec_padding_mask")

    output = transformer((enc_input, dec_input),
                         enc_padding_mask=enc_padding_mask,
                         look_ahead_mask=dec_target_padding_mask,
                         dec_padding_mask=dec_padding_mask)

    # we only discretize the output in the STE approach, and then we use the STE Argmax layer
    if discrete:
        output = STArgmax()(output)

    model = tf.keras.models.Model(
        inputs=[enc_input, dec_input, enc_padding_mask, dec_target_padding_mask, dec_padding_mask], outputs=output)
    return model
