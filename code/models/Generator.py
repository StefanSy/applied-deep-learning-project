# loosely based on
# https://keras.io/examples/nlp/neural_machine_translation_with_transformer/
# https://www.tensorflow.org/text/tutorials/transformer
from models.layers.transformer import Transformer
from tensorflow.keras import layers
import tensorflow as tf

from models.layers.transformer import positional_encoding

from models.layers.transformer import EncoderLayer

from models.layers.transformer import DecoderLayer


class Generator(Transformer):
    """
    The class represents a generator based on a transformer model. However, unlike an ordinary transformer,
    the same embedding should be used for the encoder and decoder parts, since the sentences
    in a chatbot's environment are in the same language for question and answer sentences.
    (For testing purposes, however, this was overridden here (see embedding2),
    as the model was tested on a language translation dataset).

    This generator model is used for the fully continuous text generator approach.
    In addition, the output should not be discretized to words, but should remain as a vector representation.
    (in this case embed_dim = target_size)
    """

    def __init__(self, num_layers, embed_dim, dense_dim, num_heads, vocab_size, target_size,
                 maximum_position_encoding=10000, do_rate=0.1, **kwargs):
        super(Generator, self).__init__(num_layers, embed_dim, dense_dim, num_heads,
                                        vocab_size, vocab_size, target_size,
                                        maximum_position_encoding, maximum_position_encoding, do_rate, **kwargs)

        self.embedding = Embedding(embed_dim, vocab_size, maximum_position_encoding, do_rate)
        self.embedding2 = Embedding(embed_dim, vocab_size, maximum_position_encoding, do_rate)  # TODO for test purposes

        self.encoder = Encoder(num_layers, embed_dim, dense_dim, num_heads, self.embedding, do_rate)
        self.decoder = Decoder(num_layers, embed_dim, dense_dim, num_heads, self.embedding2, do_rate)

    def call(self, enc_input, dec_input, training,
             enc_padding_mask=None, look_ahead_mask=None, dec_padding_mask=None, *args, **kwargs):
        enc_output = self.encoder(enc_input, training, enc_padding_mask)
        dec_output = self.decoder(dec_input, enc_output, training, look_ahead_mask, dec_padding_mask)
        final_output = self.final_layer(dec_output)
        return final_output


class Embedding(layers.Layer):
    def __init__(self, embed_dim, vocab_size, maximum_position_encoding, do_rate=0.1, **kwargs):
        super(Embedding, self).__init__(**kwargs)

        self.embed_dim = embed_dim
        self.vocab_size = vocab_size
        self.maximum_position_encoding = maximum_position_encoding

        self.embedding = layers.Embedding(vocab_size, embed_dim)
        self.pos_encoding = positional_encoding(maximum_position_encoding, embed_dim)
        self.dropout = layers.Dropout(do_rate)

    def call(self, inputs, training=None):
        seq_len = tf.shape(inputs)[1]

        x = self.embedding(inputs)
        x *= tf.math.sqrt(tf.cast(self.embed_dim, tf.float32))
        x += self.pos_encoding[:, :seq_len, :]
        x = self.dropout(x, training=training)

        return x


class Encoder(layers.Layer):
    def __init__(self, num_layers, embed_dim, dense_dim, num_heads, embedding, do_rate=0.1,
                 **kwargs):
        super(Encoder, self).__init__(**kwargs)
        self.num_layers = num_layers
        self.embed_dim = embed_dim
        self.dense_dim = dense_dim
        self.num_heads = num_heads
        self.do_rate = do_rate

        self.embedding = embedding

        self.enc_layers = [EncoderLayer(embed_dim, dense_dim, num_heads, do_rate) for _ in range(num_layers)]

    def call(self, inputs, training, mask=None, *args, **kwargs):
        x = self.embedding(inputs, training)

        for i in range(self.num_layers):
            x = self.enc_layers[i](x, training, mask)

        return x


class Decoder(layers.Layer):
    def __init__(self, num_layers, embed_dim, dense_dim, num_heads, embedding, do_rate=0.1,
                 **kwargs):
        super(Decoder, self).__init__(**kwargs)

        self.num_layers = num_layers
        self.dense_dim = dense_dim
        self.num_heads = num_heads
        self.embed_dim = embed_dim
        self.do_rate = do_rate

        self.embedding = embedding

        self.dec_layers = [DecoderLayer(embed_dim, dense_dim, num_heads, do_rate) for _ in range(num_layers)]

    def call(self, inputs, enc_outputs, training, look_ahead_mask, padding_mask, *args, **kwargs):
        seq_len = tf.shape(inputs)[1]

        x = self.embedding(inputs, training)

        for i in range(self.num_layers):
            x = self.dec_layers[i](x, enc_outputs, training, look_ahead_mask, padding_mask)

        return x
