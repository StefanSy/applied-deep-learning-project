# from https://www.tensorflow.org/tutorials/text/word2vec
import tensorflow as tf
import tqdm
from models.layers.Embedding import Embedding

SEED = 42
AUTOTUNE = tf.data.AUTOTUNE


def generate_training_data(sequences, window_size, num_ns, vocab_size, seed):
    """
    Generates skip-gram pairs with negative sampling.
    :param sequences: list of sequences (int-encoded sentences)
    :param window_size: window size the sampling is based on
    :param num_ns: number of negative samples
    :param vocab_size: vocabulary size
    :param seed:
    :return: skip-gram dataset consisting of lists for word pair (target_word, context_word) and corresponding labels
    """

    # Elements of each training example are appended to these lists.
    targets, contexts, labels = [], [], []

    # Build the sampling table for vocab_size tokens.
    sampling_table = tf.keras.preprocessing.sequence.make_sampling_table(vocab_size)

    # Iterate over all sequences (sentences) in dataset.
    for sequence in tqdm.tqdm(sequences):

        # Generate positive skip-gram pairs for a sequence (sentence).
        positive_skip_grams, _ = tf.keras.preprocessing.sequence.skipgrams(
            sequence,
            vocabulary_size=vocab_size,
            sampling_table=sampling_table,
            window_size=window_size,
            negative_samples=0)

        # Iterate over each positive skip-gram pair to produce training examples
        # with positive context word and negative samples.
        for target_word, context_word in positive_skip_grams:
            context_class = tf.expand_dims(
                tf.constant([context_word], dtype="int64"), 1)
            negative_sampling_candidates, _, _ = tf.random.log_uniform_candidate_sampler(
                true_classes=context_class,
                num_true=1,
                num_sampled=num_ns,
                unique=True,
                range_max=vocab_size,
                seed=seed,
                name="negative_sampling")

            # Build context and label vectors (for one target word)
            negative_sampling_candidates = tf.expand_dims(
                negative_sampling_candidates, 1)

            context = tf.concat([context_class, negative_sampling_candidates], 0)
            label = tf.constant([1] + [0] * num_ns, dtype="int64")

            # Append each element from the training example to global lists.
            targets.append(target_word)
            contexts.append(context)
            labels.append(label)

    return targets, contexts, labels


class Word2Vec(tf.keras.Model):
    """
    This class Word2Vec represents the model for pretraining the embedding weights
    by classifying word pairs if they match or not.
    """
    def __init__(self, vocab_size, embedding_dim, num_ns, one_hot):
        super(Word2Vec, self).__init__()
        self.target_embedding = Embedding(vocab_size, embedding_dim)
        self.context_embedding = tf.keras.layers.Embedding(vocab_size,
                                                           embedding_dim,
                                                           input_length=num_ns + 1)
        self.one_hot = one_hot

    def call(self, pair):
        """
        The model processes the word pair by a embedding for each word.
        The result of the word vectors are then merged by the dot product. The output is then used for classification.
        :param pair: tuple of target and context word
        :return: result of dot product
        """
        target, context = pair
        # target: (batch, dummy?)  # The dummy axis doesn't exist in TF2.7+
        # context: (batch, context)
        if len(target.shape) == 2:
            target = tf.squeeze(target, axis=1)
        # target: (batch,)
        word_emb = self.target_embedding(target, self.one_hot)
        # word_emb: (batch, embed)
        context_emb = self.context_embedding(context)
        # context_emb: (batch, context, embed)
        dots = tf.einsum('be,bce->bc', word_emb, context_emb)
        # dots: (batch, context)
        return dots

    def get_embedding_layer(self):
        return self.target_embedding
