import tensorflow as tf


class Embedding(tf.keras.layers.Layer):
    """
    This class implements a own version of an embedding layer.
    In contrast to a usual Embedding layer from tensorflow this class makes use of a Dense layer
    to get the same functionality, with the advantage that this layer is differentiable an can be used to pass
    the gradient through the Embedding layer during backpropagation
    """

    def __init__(self, vocab_size, embedding_dim):
        super(Embedding, self).__init__()
        self.vocab_size = vocab_size
        self.dense = tf.keras.layers.Dense(embedding_dim, use_bias=False)

    def call(self, input, one_hot=True):
        if not one_hot:
            input = tf.one_hot(input, self.vocab_size)
        return self.dense(input)

    def set_weights(self, weights):
        self.dense.set_weights(weights)

    def get_weights(self):
        return self.dense.get_weights()
