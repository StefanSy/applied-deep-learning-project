import tensorflow as tf


class MaskingLayer(tf.keras.layers.Layer):
    """
    This layer creates the padding and look ahead mask to the input of encoder and decoder.
    Because this processing of data takes place in a layer, the model can be trained
    without adjusting the usual tensorflow training loop.
    """

    def create_padding_mask(self, seq):
        """
        creates a mask for the places where no more words follow,
        because the sentence is already over (padding of words).
        The mask has a 0 if a word is given (value is not equal to 0) and 1 if a word is missing.
        :param seq: sequence to apply the mask
        :return: padding mask
        """
        seq = tf.cast(tf.math.equal(seq, 0), tf.float32)
        return seq[:, tf.newaxis, tf.newaxis, :]

    def create_look_ahead_mask(self, size):
        """
        creates a mask that excludes words that are not yet known at the current time.
        If the second word of a sentence is predicted, only previous words, i.e. only the first word,
        can be used for prediction, all future words are ignored. Otherwise, the network would only learn
        to pass on the current word from the sentence and would not infer this from the context as intended.
        :param size: size of the sequence
        :return: look ahead mask
        """
        mask = 1 - tf.linalg.band_part(tf.ones((size, size)), -1, 0)
        return mask  # (seq_len, seq_len)

    def call(self, enc_embed, dec_embed, with_lookahead=False):
        """
        creates padding and look ahead masks for the encoder and decoder input
        :param enc_embed: input from the encoder
        :param dec_embed: target from the decoder
        :param with_lookahead: boolean value that decides whether the look ahead mask is applied to decoder
        :return: padding mask of the encoder and decoder and look ahead mask for the decoder
        """
        enc_padding_mask = self.create_padding_mask(enc_embed)
        dec_padding_mask = self.create_padding_mask(enc_embed)
        dec_target_padding_mask = self.create_padding_mask(dec_embed)

        if with_lookahead:
            look_ahead_mask = self.create_look_ahead_mask(tf.shape(dec_embed)[1])
            # combine the masks using the maximum of both masks such that the 1 of the masks is dominant
            # which means that a word that is masked by at least one mask is also masked by the combined mask
            combined_mask = tf.maximum(dec_target_padding_mask, look_ahead_mask)
            return enc_padding_mask, combined_mask, dec_padding_mask
        else:
            return enc_padding_mask, dec_target_padding_mask, dec_padding_mask
