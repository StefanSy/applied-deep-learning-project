import tensorflow as tf
import tensorflow_probability as tfp

class STArgmax(tf.keras.layers.Layer):
    '''
    Implementation of the Straight-Through Estimator Argmax function.
    This allows us to differentiate a discrete functin
    '''
    def __init__(self, tau=1.0):
        '''
        initializes the layer with the temperature tau and a gumbel distribution to sample from
        :param tau: temperature for gumbel softmax
        '''
        super(STArgmax, self).__init__()
        self.tau = tau
        self.gumbel = tfp.distributions.Gumbel(0, 1)

    # tf.custom_gradient allows us to overwrite the gradient of a layer
    @tf.custom_gradient
    def call(self, probs, axis=-1):
        '''
        for the forward pass calculates the argmax along the axis given as parameter but also calculates the gumbel softmax.
        The gradient of the softmax function is defined by itself, which is why we can use this result in the backward pass.
        :param probs: input probability distributin
        :param axis: on which axis the argmax should be performed
        :return: the argmax along the axis as onehot and the function for the backward pass
        '''

        # one approach was to let tensorflow calculate the gradient for us
        # however since the gradient of the softmax function has a very special format it is actually faster to
        # calculate the gradient ourselves
            #with tf.GradientTape() as tape:
            #    tape.watch(probs)
            #    fake_out = tf.math.softmax((tf.math.log(probs) + self.gumbel.sample(probs_shape)) / self.tau, axis=axis)
            # gradient = tape.gradient(fake_out, probs)

        probs_shape = tf.shape(probs)
        # we add a very small number to the incoming probabilities, because we have to calculate the log of the
        # input, which is only defined for values greater zero
        probs = probs + (1e-7 / tf.cast(probs_shape[-1], tf.float32))
        # formula of gumbel softmax
        fake_out = tf.math.softmax((tf.math.log(probs) + self.gumbel.sample(probs_shape)) / self.tau, axis=axis)
        # actual argmax function
        real_out = tf.one_hot(tf.math.argmax(probs, axis=axis), probs.shape[axis])

        def grad(dy):
            # replaces a matrix-vector multiplication which is in O(n^2) to elementwise vector operations which are in O(n)
            # we can use the special structure of the softmax jacobian to turn it into vector operations
            return (fake_out / (self.tau * probs)) * (dy - tf.reduce_sum(dy * fake_out))
            # which is faster than using the Gradient Tape approach
                #return dy * gradient

        return real_out, grad
