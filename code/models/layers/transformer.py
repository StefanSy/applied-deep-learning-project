import tensorflow as tf
import numpy as np
from tensorflow import keras
from tensorflow.keras import layers
from models.layers.MultiHeadAttention import MultiHeadAttention


# we did not comment code because this is directly from the tensorflow tutorial where everything is better explained in detail
# the only thing we changed is, that we removed the embedding from the Transformer and embed in the GAN training loop
# we do this, since we want to use the same embedding in the whole GAN.
# code from keras and tensorflow transformer tutorial
# https://keras.io/examples/nlp/neural_machine_translation_with_transformer/
# https://www.tensorflow.org/text/tutorials/transformer

class Transformer(keras.Model):
    """
    This class implements a Transformer model with all its components: Encoder, Decoder and final layer
    """

    def __init__(self, num_layers, embed_dim, dense_dim, num_heads, vocab_size, target_size, pe_input=10000, pe_target=10000, do_rate=0.1, **kwargs):
        super(Transformer, self).__init__(**kwargs)

        self.encoder = Encoder(num_layers, embed_dim, dense_dim, num_heads, vocab_size, pe_input, do_rate)
        self.decoder = Decoder(num_layers, embed_dim, dense_dim, num_heads, vocab_size, pe_target, do_rate)

        self.final_layer = layers.Dense(target_size)

    def call(self, inputs, training=None, enc_padding_mask=None, look_ahead_mask=None, dec_padding_mask=None):
        enc_input, dec_input = inputs
        enc_output = self.encoder(enc_input, training, enc_padding_mask)
        dec_output = self.decoder(dec_input, enc_output, training, look_ahead_mask, dec_padding_mask)
        final_output = self.final_layer(dec_output)
        return final_output


class Encoder(layers.Layer):
    def __init__(self, num_layers, embed_dim, dense_dim, num_heads, vocab_size, maximum_position_encoding, do_rate=0.1, **kwargs):
        super(Encoder, self).__init__(**kwargs)
        self.num_layers = num_layers
        self.embed_dim = embed_dim
        self.dense_dim = dense_dim
        self.num_heads = num_heads
        self.vocab_size = vocab_size
        self.maximum_position_encoding = maximum_position_encoding
        self.do_rate = do_rate

        # self.embedding = layers.Embedding(vocab_size, embed_dim, trainable=False)
        self.pos_encoding = positional_encoding(maximum_position_encoding, embed_dim)

        self.enc_layers = [EncoderLayer(embed_dim, dense_dim, num_heads, do_rate) for _ in range(num_layers)]

        self.dropout = layers.Dropout(do_rate)

    def call(self, inputs, training, mask=None):
        seq_len = tf.shape(inputs)[1]

        # since we want to use the same embedding everywhere we put it into the GAN class
        # x = self.embedding(inputs)
        x = inputs
        x *= tf.math.sqrt(tf.cast(self.embed_dim, tf.float32))
        x += self.pos_encoding[:, :seq_len, :]

        x = self.dropout(x, training=training)

        for i in range(self.num_layers):
            x = self.enc_layers[i](x, training, mask)

        return x


class Decoder(layers.Layer):
    def __init__(self, num_layers, embed_dim, dense_dim, num_heads, vocab_size, maximum_position_encoding, do_rate=0.1, **kwargs):
        super(Decoder, self).__init__(**kwargs)

        self.num_layers = num_layers
        self.embed_dim = embed_dim
        self.dense_dim = dense_dim
        self.num_heads = num_heads
        self.vocab_size = vocab_size
        self.maximum_position_encoding = maximum_position_encoding
        self.do_rate = do_rate

        # self.embedding = layers.Embedding(vocab_size, embed_dim, trainable=False)
        self.pos_encoding = positional_encoding(maximum_position_encoding, embed_dim)

        self.dec_layers = [DecoderLayer(embed_dim, dense_dim, num_heads, do_rate) for _ in range(num_layers)]
        self.dropout = layers.Dropout(do_rate)

    def call(self, inputs, enc_outputs, training, look_ahead_mask, padding_mask):
        seq_len = tf.shape(inputs)[1]

        # since we want to use the same embedding everywhere we put it into the GAN class
        # x = self.embedding(inputs)
        x = inputs
        x *= tf.math.sqrt(tf.cast(self.embed_dim, tf.float32))
        x += self.pos_encoding[:, :seq_len, :]

        x = self.dropout(x, training=training)

        for i in range(self.num_layers):
            x = self.dec_layers[i](x, enc_outputs, training, look_ahead_mask, padding_mask)

        return x

class EncoderLayer(layers.Layer):
    def __init__(self, embed_dim, dense_dim, num_heads, do_rate=0.1, **kwargs):
        super(EncoderLayer, self).__init__(**kwargs)
        self.embed_dim = embed_dim
        self.dense_dim = dense_dim
        self.num_heads = num_heads
        self.do_rate = do_rate

        self.attention = MultiHeadAttention(num_heads=num_heads, key_dim=embed_dim)
        self.dense_proj = keras.Sequential([layers.Dense(dense_dim, activation='relu'), layers.Dense(embed_dim)])

        self.layernorm_1 = layers.LayerNormalization(epsilon=1e-6)
        self.layernorm_2 = layers.LayerNormalization(epsilon=1e-6)

        self.dropout_1 = layers.Dropout(do_rate)
        self.dropout_2 = layers.Dropout(do_rate)

    def call(self, inputs, training, mask = None):
        attention_output = self.attention(query=inputs, value=inputs, key=inputs, attention_mask=mask)
        attention_output = self.dropout_1(attention_output, training=training)
        proj_input = self.layernorm_1(inputs + attention_output)

        proj_output = self.dense_proj(proj_input)
        proj_output = self.dropout_2(proj_output, training=training)
        return self.layernorm_2(proj_input + proj_output)


class DecoderLayer(layers.Layer):
    def __init__(self, embed_dim, latent_dim, num_heads, do_rate=0.1, **kwargs):
        super(DecoderLayer, self).__init__(**kwargs)
        self.embed_dim = embed_dim
        self.latent_dim = latent_dim
        self.num_heads = num_heads
        self.do_rate = do_rate

        self.attention_1 = MultiHeadAttention(num_heads=num_heads, key_dim=embed_dim)
        self.attention_2 = MultiHeadAttention(num_heads=num_heads, key_dim=embed_dim)
        self.dense_proj = keras.Sequential([layers.Dense(latent_dim, activation='relu'), layers.Dense(embed_dim)])

        self.layernorm_1 = layers.LayerNormalization(epsilon=1e-6)
        self.layernorm_2 = layers.LayerNormalization(epsilon=1e-6)
        self.layernorm_3 = layers.LayerNormalization(epsilon=1e-6)

        self.dropout_1 = layers.Dropout(do_rate)
        self.dropout_2 = layers.Dropout(do_rate)
        self.dropout_3 = layers.Dropout(do_rate)

    def call(self, inputs, encoder_outputs, training, look_ahead_mask = None, padding_mask = None):
        attention_output_1 = self.attention_1(query=inputs, value=inputs, key=inputs, attention_mask=look_ahead_mask)
        attention_output_1 = self.dropout_1(attention_output_1, training=training)
        out_1 = self.layernorm_1(inputs + attention_output_1)

        attention_output_2 = self.attention_2(query=out_1, value=encoder_outputs, key=encoder_outputs,
                                              attention_mask=padding_mask)
        attention_output_2 = self.dropout_2(attention_output_2, training=training)
        out_2 = self.layernorm_2(out_1 + attention_output_2)

        proj_output = self.dense_proj(out_2)
        proj_output = self.dropout_3(proj_output, training=training)
        return self.layernorm_3(out_2 + proj_output)


def get_angles(pos, i, embed_dim):
    angle_rates = 1 / np.power(10000, (2 * (i // 2)) / np.float32(embed_dim))
    return pos * angle_rates


def positional_encoding(position, embed_dim):
    angle_rads = get_angles(np.arange(position)[:, np.newaxis],
                            np.arange(embed_dim)[np.newaxis, :],
                            embed_dim)

    angle_rads[:, 0::2] = np.sin(angle_rads[:, 0::2])
    angle_rads[:, 1::2] = np.cos(angle_rads[:, 1::2])

    pos_encoding = angle_rads[np.newaxis, ...]

    return tf.cast(pos_encoding, dtype=tf.float32)
