import tensorflow as tf
from models.layers.MaskingLayer import MaskingLayer
from models.Generator import Generator


def get_generator(num_layers, embed_dim, dense_dim, num_heads, dropout_rate,
                  maximum_position_encoding=1000, vocab_size=10000):
    """
    creates the generator model for the fully continuous text generator approach
    :param num_layers:
    :param embed_dim:
    :param dense_dim:
    :param num_heads:
    :param dropout_rate:
    :param maximum_position_encoding:
    :param vocab_size:
    :return: overall generator model with masking and generator itself
    """

    generator = Generator(
        num_layers=num_layers,
        embed_dim=embed_dim,
        dense_dim=dense_dim,
        num_heads=num_heads,
        vocab_size=vocab_size,
        target_size=embed_dim,  # target_size = embed_dim (fully continuous text generator approach)
        maximum_position_encoding=maximum_position_encoding,
        do_rate=dropout_rate
    )

    enc_input = tf.keras.Input(shape=(None,), dtype=tf.int32)
    dec_input = tf.keras.Input(shape=(None,), dtype=tf.int32)
    enc_padding_mask, dec_target_padding_mask, dec_padding_mask = MaskingLayer()(enc_input, dec_input, False)
    output = generator(enc_input, dec_input,
                       enc_padding_mask=enc_padding_mask,
                       look_ahead_mask=dec_target_padding_mask,
                       dec_padding_mask=dec_padding_mask)

    model = tf.keras.models.Model(inputs=[enc_input, dec_input], outputs=output)
    return model, generator
