# based on https://www.tensorflow.org/tutorials/text/word2vec
import numpy as np
from models.Word2Vec import *
from tensorflow.keras.layers.experimental.preprocessing import TextVectorization


def train_and_save_embedding_and_vectorize(vocab_size=4096):
    '''
    trains our custom embedding layer and the corresponding tokenizer and saves them for later use on the cornell movie dialogue data set
    :param vocab_size: max vocab set, only the most common used receive ther own token, all other get a "other" token
    :return: nothing, saves embedding and tokenizer to disk
    '''
    num_ns = 4

    cornell_data = tf.data.experimental.load('./data/cornell')

    # dataset contains dialogues so two sentences per sample. for the embedding/tokenizer training we only need one input
    # hence we just take each sentence as its own sample
    ds = cornell_data.batch(1024).map(lambda a, b: tf.concat([a, b], -1)).unbatch()

    # create and adapt tokenizer
    vectorize_layer = TextVectorization(max_tokens=vocab_size, output_sequence_length=32)
    vectorize_layer.adapt(ds.batch(1024))

    # tokenize dataset
    text_vector_ds = ds.batch(1024).map(vectorize_layer).unbatch()

    # get training batches for embedding training (see https://www.tensorflow.org/tutorials/text/word2vec)
    sequences = list(text_vector_ds.as_numpy_iterator())
    targets, contexts, labels = generate_training_data(
        sequences=sequences,
        window_size=2,
        num_ns=num_ns,
        vocab_size=vocab_size,
        seed=SEED)

    targets = np.array(targets)
    contexts = np.array(contexts)[:, :, 0]
    labels = np.array(labels)

    BATCH_SIZE = 1024
    BUFFER_SIZE = 10000
    dataset = tf.data.Dataset.from_tensor_slices(((targets, contexts), labels))
    dataset = dataset.shuffle(BUFFER_SIZE).batch(BATCH_SIZE, drop_remainder=True).cache().prefetch(buffer_size=AUTOTUNE)

    embedding_dim = 128
    word2vec = Word2Vec(vocab_size, embedding_dim, num_ns, False)
    word2vec.compile(optimizer='adam',
                     loss=tf.keras.losses.CategoricalCrossentropy(from_logits=True),
                     metrics=['accuracy'])

    word2vec.fit(dataset, epochs=30)

    embedding_layer = word2vec.get_embedding_layer()

    # get weights from tokenizer and embedding and save the weights to disk
    embedding_weights = embedding_layer.get_weights()
    vectorize_weights = vectorize_layer.get_weights()

    np.save("./layer_weights/embedding_weights", embedding_weights)
    np.save("./layer_weights/vectorize_layer_weights", vectorize_weights)


def load_embedding_and_vectorize_layer(path, vocab_size, embedding_dim):
    '''
    loads the weights for embedding and tokenizer and creates pre-trained tokenize and embedding layer
    :param path: the folder where the weights are stored
    :param vocab_size: same vocab size when training the embedding
    :param embedding_dim: same embedding size when training the embedding
    :return: pre-trained embedding and tokenize layer, loaded from disk
    '''

    # load the weights from disk
    embedding_weights = np.load(path + "/embedding_weights.npy", allow_pickle=True)
    vectorize_weights = np.load(path + "/vectorize_layer_weights.npy", allow_pickle=True)

    # create "empty" layers
    embedding_layer = Embedding(vocab_size, embedding_dim)
    vectorize_layer = TextVectorization(max_tokens=vocab_size, output_sequence_length=128)

    # we cannot overwrite the weights of layers until they already have a set of weights
    # layers only get a set of weights when they are called at least once
    # which is why we have to call the layers here with some random dummy input
    embedding_layer(np.random.randint(low=0, high=1, size=vocab_size), False)
    # You have to call `adapt` with some dummy data (BUG in Keras) see https://stackoverflow.com/questions/65103526/how-to-save-textvectorization-to-disk-in-tensorflow/65225240
    vectorize_layer.adapt(tf.data.Dataset.from_tensor_slices(["xyz"]))

    # after that we can actually overwrite the weights..
    embedding_layer.set_weights(embedding_weights)
    vectorize_layer.set_weights([vectorize_weights[0], vectorize_weights[1].astype('int64')])

    # ... and return the layers
    return embedding_layer, vectorize_layer
